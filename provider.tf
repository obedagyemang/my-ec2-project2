provider "aws" {
  profile = "default"
  shared_credentials_file = "$HOME/.aws/credentials"
  region  = var.region_id
}