resource "aws_instance" "ubuntu" {
    count = 1
  key_name      = "my-keypair"
  ami           = "ami-0be2609ba883822ec"
  instance_type = "t2.micro"

  tags = {
   Name = var.tags
  }
}